/*
1. Escreva uma função JavaScript que inverta um número. 
Entrada Esperada: 32243;
Saída Esperada: 34223	
*/

// Utilizando funções do ES6
function inverseNumber(num){
	return Number(num.toString().split("").reverse().join(""));
}

// Utilizando um for simples
function inverseNumberFor(num){
	const numString = num.toString();
  let numReverseString = "";
  
  for(let i = numString.length -1 ; i >= 0 ; i--){
  	numReverseString += numString[i]
  }
  return Number(numReverseString);
}

console.log(inverseNumber(12345));
console.log(inverseNumber(1122334455));
console.log(inverseNumber(56831));

console.log("------------------")

console.log(inverseNumberFor(12345));
console.log(inverseNumberFor(1122334455));
console.log(inverseNumberFor(56831));